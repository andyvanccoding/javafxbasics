package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.Month;
import java.util.ResourceBundle;

public class TableViewController implements Initializable {


    @FXML private TableView<Person> tableView;

    @FXML private TableColumn<Person, String> firstNameColumn;

    @FXML private TableColumn<Person, String> lastNameColumn;

    @FXML private TableColumn <Person, LocalDate> birthdayColumn;

    @FXML private TextField firstNameTextField;

    @FXML private TextField lastNameTextField;

    @FXML private DatePicker birthdayDatePicker;




    /*
     * this methods will allow to change the first and last name in te column
     */
    public void changeFirstNameCellEvent(TableColumn.CellEditEvent cellEditEvent)
    {
            Person personSelected = tableView.getSelectionModel().getSelectedItem();
            personSelected.setFirstName(cellEditEvent.getNewValue().toString());
    }

    public void changeLastNameCellEvent(TableColumn.CellEditEvent cellEditEvent)
    {
        Person personSelected = tableView.getSelectionModel().getSelectedItem();
        personSelected.setLastName(cellEditEvent.getNewValue().toString());
    }

    /*
     * this method will create a new person object in the table
     */
    public void newPersonButtonPushed()
    {
        Person newPerson = new Person(firstNameTextField.getText(),
                                        lastNameTextField.getText(),
                                        birthdayDatePicker.getValue());

        //get all items from the table, then add the new person to the list.
        tableView.getItems().add(newPerson);

    }

    /*
     *this method will remove a person
     */
    public void deleteButtonPushed() {
        ObservableList<Person> selectedRows, allPeople;
        allPeople = tableView.getItems();

        //this gives the rows that were selected
        selectedRows = tableView.getSelectionModel().getSelectedItems();
        //loop over list and remove person
        for (Person person : selectedRows)
        {
            allPeople.remove(person);
        }
    }


    /*
     * change the scene to Canvas
     */
    public void changeScreenButtonPushedToTableView(ActionEvent event) throws IOException
    {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("Canvas.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
        window.setScene(tableViewScene);
        window.show();

//		Stage newWindow = new Stage();
//		newWindow.setScene(tableViewScene);
//		newWindow.show();

    }

    //method will pass selected person object to detailed view
    public void changeSceneToDetailedPersonView(ActionEvent event) throws IOException
    {
        Person selectedPerson = tableView.getSelectionModel().getSelectedItem();
        if(selectedPerson != null) {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("PersonView.fxml"));
            Parent tableViewParent = loader.load();
            Scene tableViewScene = new Scene(tableViewParent);

            //access controller and call method
            PersonViewController controller = loader.getController();  //this gets the controller
            //this calls the method from the OTHER controller object
            // and inputs param data from THIS controller object
            //thus passing data between scenes
            controller.initData(selectedPerson);


            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setScene(tableViewScene);
            window.show();
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        // set up the columns in the table
        firstNameColumn.setCellValueFactory(new PropertyValueFactory("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory("lastName"));
        birthdayColumn.setCellValueFactory(new PropertyValueFactory("birthday"));

        //make tableView and columns editable
        tableView.setEditable(true);
        firstNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        lastNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());

        //this will allow the table to select multiple rows at once
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);


        //load dummy data
        tableView.setItems(getPeople());
    }

    private ObservableList getPeople() {
        ObservableList<Person> people = FXCollections.observableArrayList();
        people.add(new Person("Frank", "Sinatra", LocalDate.of(1915, Month.DECEMBER, 12)));
        people.add(new Person("Rebecca", "Fergusson", LocalDate.of(1986, Month.JULY, 21)));
        people.add(new Person("Mr.", "T", LocalDate.of(1952, Month.MAY, 21)));

        return people;
    }
}
